# AdventureWorks Products Management Database

AdventureWorks Products Management is a platform where you can see all products stored in database and add more products thanks to a simple form.

### Informations
* This project works with **[Docker-Compose](https://docs.docker.com/compose/install/)**.

### Installation

#### Installation and container launch from *command line*.

+ First, please clone the project :
````bash
git clone https://gitlab.com/BiggyDev/rattrapage-projet-annuel-db.git
````

+ Move forward inside the project folder :
````bash
cd rattrapage-projet-annuel-db
````

+ Launch the container :

````bash
docker-compose up -d
````

### **Database visualisation thanks to [PhpMyAdmin](https://www.phpmyadmin.net/).**

### Usage

+ Open your favorite browser at [http://localhost:82](http://localhost:82/).
+ Navigate through the AdventureWorks database by selecting : adventureworks
